#!/usr/bin/env python3
# coding: utf-8

from .crc16 import crc16

class Wifibot:

    RESPONSE_LENGTH = 21
    PORT = 15020
    REFRESH_TIME = 100

    VOLTAGE_MAX = 18
    VOLTAGE_LIMIT = 11

    SPEED_MAX = 360
    SPEED_DEFAULT = 200

    MOTOR_CONTROL_PID_10 = False

    BATTERY_FULL = 17

    IR_MAX = 255
    IT_LIMIT = 60

    def rotate(self, speed, clock):
        res = bytearray()
        res[0] = 0xff
        res[1] = 0x07
        res[2] = speed
        res[3] = speed >> 8
        res[4] = speed
        res[5] = speed >> 8

        if clock:
            res[6] = 0x43
        else:
            res[6] = 0x13

        if self.onMotorControl:
            res[6] = res[6] | 0xa0

        if self.motor_control_pid_10:
            res[6] = res[6] | 0x08

        check = crc16(res[1:7])
        res[7], res[8] = check, check >> 8

    def direction(self, speed, right, forward):
        res = bytearray()
        res[0] = 0xff
        res[1] = 0x07

        if not right:
            res[2] = speed
            res[3] = speed >> 8
            res[4] = 0x00
            res[5] = 0x00
            if forward:
                res[6] = 0x43
            else:
                res[6] = 0x03
        else:
            res[2] = 0x00
            res[3] = 0x00

            res[4] = speed
            res[5] = speed >> 8
            if forward:
                res[6] = 0x13
            else:
                res[6] = 0x03

        if self.onMotorControl:
            res[6] = res[6] | 0xa0

        if self.motor_control_pid_10:
            res[6] = res[6] | 0x08

        check = crc16(res[1:7])
        res[7], res[8] = check, check >> 8

    def nothing():
        res = bytearray()
        res[0] = 0xff
        res[1] = 0x07
        res[2] = 0x00
        res[3] = 0x00
        res[4] = 0x00
        res[5] = 0x00
        res[6] = 0x53

        check = crc16(res[1:7])
        res[7], res[8] = check, check >> 8

    def forward(self, speed):
        res = bytearray()
        res[0] = 0xff
        res[1] = 0x07

        res[2] = speed
        res[3] = speed >> 8
        res[4] = speed
        res[5] = speed >> 8
        res[6] = 0x53

        if self.context.onMotorControl:
            res[6] = res[6] | 0xa0

        if self.WifibotLab2Activity.MOTOR_CONTROL_PID_10:
            res[6] = res[6] | 0x08

        check = crc16(res[1:7])
        res[7], res[8] = check, check >> 8

    def backward(self, speed):
        res = bytearray()
        res[0] = 0xff  # 255
        res[1] = 0x07  # size

        res[2] = speed  # left speed
        res[3] = speed>>8
        res[4] = speed  # right speed
        res[5] = speed>>8
        res[6] = 0x03  # backward

        if self.onMotorControl:
            res[6] = res[6] | 0xa0

        if self.MOTOR_CONTROL_PID_10:
            res[6] = res[6] | 0x08

        check = crc16(res[1:7])
        res[7], res[8] = check, check >> 8

    def write(self, bin_data):
        if not self.connected:
            raise Exception("Not connected")

        # check security
        if self.onSecurity:
            # block forward
            if ((bin_data[6] & 0x50) == 0x50) and (rdata[11] > self.IR_LIMIT or rdata[3] > self.IR_LIMIT):
                bin_data[2] = 0
                bin_data[3] = 0
                bin_data[4] = 0
                bin_data[5] = 0

            if ((bin_data[6] & 0x50) == 0x00) and (rdata[12] > self.IR_LIMIT or rdata[4] > self.IR_LIMIT):
                bin_data[2] = 0
                bin_data[3] = 0
                bin_data[4] = 0
                bin_data[5] = 0

        logging.info("Write :")
        logging.info("Read : " + str(rdata))

        self.voltage = rdata[2] & 0xff
        self.irFr = rdata[11] & 0xff
        self.irFl = rdata[3] & 0xff
        self.irBr = rdata[4] & 0xff
        self.irBl = rdata[12] & 0xff
        self.current = rdata[17] & 0xff
        self.batteryState = rdata[18] & 0xff
