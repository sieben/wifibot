// Robot server for the Move application (wbot_move)
// September 2011
// Milan Erdelj

#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <time.h>
#include <sys/time.h>
#include <malloc.h>

#define PID_P 95
#define PID_I 0
#define PID_D 45
#define MAX_SPEED 140
#define ROTATION_SPEED 100
#define ACCELERATION 5
#define FLAG_FORWARD 0xF0
#define FLAG_LEFT 0xB0
#define FLAG_RIGHT 0xE0

#define THREAD_COUNT 3
#define DISPLAY_COUNTER 1

#define BAUDRATE B4800
#define MODEMDEVICE "/dev/ttyUSB0"
#define MODEMDEVICE2 "/dev/ttyUSB1"
#define _POSIX_SOURCE 1

#define PORT_IN 15000
#define PORT_OUT 15050
//#define PORT_GPS 15055
#define ADDR_OUT "127.0.0.1"

#define THREAD_IN_SLEEP 10000
//#define THREAD_OUT_SLEEP 100000
#define MAIN_SLEEP 1000 // was 10000
//#define GPS_SLEEP 1000000

#define BUFF_IN_LEN 5
#define BUFF_OUT_LEN 21
//#define BUFF_GPS_LEN 8

#define debug_message

typedef unsigned char uchar;

/*
   typedef struct
   {
   char time[11];      // hhmmss.sss
   char status;        // A valid position, V navigation receiver warning
   char latitude[12];  // latitude in dddmm.mmmm
   char lat_hem;       // latitude hemisphere indicator, N north, S south
   char longitude[12]; // longitude in dddmm.mmmm
   char long_hem;      // longitude hemisphere indicator, E east, W west
   char speed_knots[5];// speed over ground (knots)
   char speed[6];      // speed in km/h
   char course[7];     // course over ground (degrees)
   char course_mag[7];
   char date[6];       // ddmmyy
   char mag_var[5];    // magnetic variation, 000.0 - 180.0 degrees
   char mag_var_dir;   // magnetic variation direction, E east, W west
   char mode;          // mode indicator, N data invalid, D differential, A autonomous, E estimated
   long l_long, l_lat;
   } GPSData;
   */

typedef struct
{
    long odom;
    int speed;
    uchar voltage;
    uchar IR1, IR2;
} SensorData;

typedef struct
{
    SensorData left, right;
    uchar current;
    uchar version;
    float pos_x, pos_y, angle;
    float dist_from_start;
    long latitude, longitude;
} RobotData;

RobotData me;
SensorData dataL, dataR;
//GPSData gps;
uchar buff_out[BUFF_OUT_LEN];

uchar speed1=0, speed2=0;
struct termios oldoptions,newoptions;

pthread_t threads[THREAD_COUNT];
int i, ret, hUSB=0, fd;

FILE *file_out;
char file_name[60],file_line[120];
char local_time[25];
time_t now;
struct timeval tv;
struct timezone tz;
struct tm *tm;

void * th_in(void *num);
//void * th_out(void *num);
//void * th_gps(void *num);

int set_motor(int hUSB, short speed1, short speed2, uchar SpeedFlag);
int set_motor_pid(int hUSB, uchar speed1, uchar speed2, uchar pp, uchar ii, uchar dd, short maxspeed);
int stop_motor(int hUSB);
//int get_motor_data(int hUSB, SensorData *dataL, SensorData *dataR);
int get_motor_buff(int hUSB);
int set_motor_low_res(int hUSB, char speed1, char speed2);

int openrs232(const char *device, int baudrate);
int readrs232(int hUSB, uchar *buffer, unsigned int nNumberOfBytesToRead, unsigned int *lpNumberOfBytesRead);
int closers232();
int writers232(int hUSB, uchar *buffer, unsigned int nNumberOfBytesToWrite, unsigned int *lpNumberOfBytesWritten);
short crc16(uchar *adresse_tab , uchar taille_max);

void file_write(char *line);
char checksum(uchar *buff, char len);
char add_checksum(uchar *buff, char len);

//==============================
//==============================
int main(int argc, char *argv[])
{
#ifdef debug_message
    int display_counter=DISPLAY_COUNTER;
#endif
    int socket_out;
    struct sockaddr_in myaddr_out;
    printf("[wifibot - server]\n");
    time(&now);
    tm = localtime(&now);
    strftime(local_time,25,"%Y_%m_%d_%H.%M",tm);
    sprintf(file_name,"/home/wifibot/robot_log_%s",local_time);
    if(!(file_out=fopen(file_name,"wb")))
    {
        printf("Cannot open file %s\n", file_name);
        exit(1);
    }
    //hUSB=SetupI2CCommPort(0);
    hUSB = openrs232("/dev/ttyS0",19200);
    close(hUSB);
    sleep(8);
    hUSB = openrs232("/dev/ttyS0",19200);
    stop_motor(hUSB);
    set_motor_pid(hUSB,0x00,0x00,PID_P,PID_I,PID_D,360);
    if((ret = pthread_create(&threads[0], NULL, th_in, (void *) 0))!=0)
    {
        sprintf(file_line,"[ERRR] %s",strerror(ret));
        file_write(file_line);
        fclose(file_out);
        exit (1);
    }
    /*
    //check GPS
    if((fd=open(MODEMDEVICE, O_RDWR | O_NOCTTY))<0)
    {
    //perror(MODEMDEVICE);
    if((fd=open(MODEMDEVICE2, O_RDWR | O_NOCTTY))<0)
    {
    //perror(MODEMDEVICE2);
    sprintf(file_line,"_MSG No GPS device!\n");
    file_write(file_line);
    //exit(-1);
    }
    else
    {
    sprintf(file_line,"_MSG Waiting for GPS device...\n");
    file_write(file_line);
    }
    }
    if(fd>=0)
    {
    if((ret = pthread_create(&threads[1], NULL, th_gps, (void *) 0))!=0)
    {
    sprintf(file_line,"ERRR %s", strerror(ret));
    file_write(file_line);
    fclose(file_out);
    exit (1);
    }
    }
    */
    if((socket_out=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP))<0)
    {
        sprintf(file_line,"[ERRR] udp out socket error\n");
        file_write(file_line);
        fclose(file_out);
        exit(1);
    }
    memset((char *) &myaddr_out, 0, sizeof(myaddr_out));
    myaddr_out.sin_family=AF_INET;
    myaddr_out.sin_port=htons(PORT_OUT);
    myaddr_out.sin_addr.s_addr=inet_addr(ADDR_OUT);
    while(1)
    {
        get_motor_buff(hUSB);
        //usleep(1000);
        sendto(socket_out, buff_out, BUFF_OUT_LEN, 0, (struct sockaddr *) &myaddr_out, sizeof(myaddr_out));
        if(!(--display_counter))
        {
            // print the data to the file
            me.left.speed=(int)(((int)buff_out[1]<<8) + buff_out[0]);
            if(me.left.speed>32767)
                me.left.speed-=65536;
            me.left.speed=me.left.speed;
            me.right.voltage=buff_out[1];
            me.left.voltage=buff_out[2];
            me.left.IR1=buff_out[3];
            me.left.IR2=buff_out[4];
            me.left.odom=((((long)buff_out[8]<<24))+(((long)buff_out[7]<<16))+(((long)buff_out[6]<<8))+((long)buff_out[5]));
            me.right.speed=(int)((buff_out[10] << 8) + buff_out[9]);
            if (me.right.speed > 32767)
                me.right.speed-=65536;
            me.right.speed=me.right.speed;
            me.right.IR1=buff_out[11];
            me.right.IR2=buff_out[12];
            me.right.odom=((((long)buff_out[16]<<24))+(((long)buff_out[15]<<16))+(((long)buff_out[14]<<8))+((long)buff_out[13]));
            me.current=buff_out[17];
            me.version=buff_out[18];
            sprintf(file_line,"[STAT] %d %d %.1f %.1f %.3f %.3f %d %d %d %d %ld %ld %d\n", me.left.speed, me.right.speed, (float)me.right.voltage/10, (float)me.left.voltage/10, (me.current*0.194)-37.5, (me.left.voltage/10)*(-((me.current*0.194)-37)), me.left.IR1, me.left.IR2, me.right.IR1, me.right.IR2, me.left.odom, me.right.odom, me.version);
            file_write(file_line);
            /*
               printf("OUT:");
               for(i=0;i<BUFF_OUT_LEN;i++)
               printf(" %d",buff_out[i]);
               printf("\n");
               */
            display_counter=DISPLAY_COUNTER;
        }
        usleep(MAIN_SLEEP);
    }
    return 0;
}

//======================
void * th_in(void * num)
{
    int socket_in;
    struct sockaddr_in myaddr_in, client_in;
    uchar buff_in[BUFF_IN_LEN];
    socklen_t clilen=sizeof(client_in);
    if((socket_in=socket(AF_INET,SOCK_DGRAM,0))<0)
    {
        fprintf(file_out,"[ERRR] udp in socket error\n");
        fclose(file_out);
        exit(1);
    }
    memset((char *)&myaddr_in, 0, sizeof(myaddr_in));
    myaddr_in.sin_family=AF_INET;
    myaddr_in.sin_addr.s_addr=htonl(INADDR_ANY);
    myaddr_in.sin_port=htons(PORT_IN);
    if(bind(socket_in,(struct sockaddr *)&myaddr_in,sizeof(myaddr_in))<0)
    {
        fprintf(file_out,"[ERRR] udp in bind error\n");
        fclose(file_out);
        exit(1);
    }
    while(1)
    {
        recvfrom(socket_in,buff_in,BUFF_IN_LEN,0,(struct sockaddr *)&client_in,&clilen);
        //commands:
        //  hello:      <1> <1> <1> <1>
        //  set speed:  <2> <speed1> <speed2> <speed_flag>
        //  set pid:    <3> <p> <i> <d>
        if(checksum(buff_in, BUFF_IN_LEN))
        {
#ifdef debug_message
            printf("[RECV] %d %d %d %d %d\n", buff_in[0], buff_in[1], buff_in[2], buff_in[3], buff_in[4]);
#endif
            switch(buff_in[0])
            {
                case 1: //hello
                    printf("\a");
                    sprintf(file_line,"[HELLO]");
                    file_write(file_line);
                    break;
                case 2: //set speed
                    set_motor(hUSB, (short)buff_in[1], (short)buff_in[2], buff_in[3]);
                    sprintf(file_line,"[SPEED] %d %d %d\n", buff_in[1], buff_in[2], buff_in[3]);
                    file_write(file_line);
                    break;
                case 3: //set pid
                    set_motor_pid(hUSB, 0, 0, buff_in[1], buff_in[2], buff_in[3], 360);
                    sprintf(file_line,"[PID] %d %d %d\n", buff_in[1], buff_in[2], buff_in[3]);
                    file_write(file_line);
                    break;
            }
        }
        else
        {
            sprintf(file_line,"[CHK_ERROR] %d %d %d %d\n", buff_in[1], buff_in[2], buff_in[3], buff_in[4]);
            file_write(file_line);
        }
        usleep(THREAD_IN_SLEEP);
    }
}

/*
//=======================
void * th_gps(void * num)
{
int res, socket_gps;
struct termios oldtio,newtio;
char buf[255], token_temp[20];
uchar buff_gps[BUFF_GPS_LEN];
struct sockaddr_in myaddr_gps;

tcgetattr(fd,&oldtio);	//current serial port settings
bzero(&newtio, sizeof(newtio));	//clear struct for new port settings
newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
newtio.c_iflag = IGNPAR | ICRNL;
newtio.c_oflag = 0;
newtio.c_lflag = ICANON;
newtio.c_cc[VINTR]    = 0;
newtio.c_cc[VQUIT]    = 0;
newtio.c_cc[VERASE]   = 0;
newtio.c_cc[VKILL]    = 0;
newtio.c_cc[VEOF]     = 4;
newtio.c_cc[VTIME]    = 0;
newtio.c_cc[VMIN]     = 1;
newtio.c_cc[VSWTC]    = 0;
newtio.c_cc[VSTART]   = 0;
newtio.c_cc[VSTOP]    = 0;
newtio.c_cc[VSUSP]    = 0;
newtio.c_cc[VEOL]     = 0;
newtio.c_cc[VREPRINT] = 0;
newtio.c_cc[VDISCARD] = 0;
newtio.c_cc[VWERASE]  = 0;
newtio.c_cc[VLNEXT]   = 0;
newtio.c_cc[VEOL2]    = 0;
//clean the modem line and activate the settings
tcflush(fd, TCIFLUSH);
tcsetattr(fd,TCSANOW,&newtio);
//send gps position localhost, port PORT_GPS
if((socket_gps=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP))<0)
{
sprintf(file_line,"ERRR gps out socket error\n");
file_write(file_line);
fclose(file_out);
exit(1);
}
memset((char *) &myaddr_gps, 0, sizeof(myaddr_gps));
myaddr_gps.sin_family=AF_INET;
myaddr_gps.sin_port=htons(PORT_GPS);
myaddr_gps.sin_addr.s_addr=inet_addr(ADDR_OUT);
while(1)
{
res = read(fd,buf,255);
buf[res]=0;	//set end of the string
//printf("%s",buf);
if(strstr(buf,"$GPRMC")!=NULL)
{
sprintf(token_temp,"%s",strtok(buf,","));   // sentence type
sprintf(token_temp,"%s",strtok(NULL,","));  //time
strcpy(gps.time,token_temp);
sprintf(token_temp,"%s",strtok(NULL,","));  //status
gps.status=token_temp[0];
if(token_temp[0]=='A')
{
sprintf(token_temp,"%s",strtok(NULL,","));  //latitude
strcpy(gps.latitude, token_temp);
sprintf(token_temp,"%s",strtok(NULL,","));   //hemisphere
sprintf(token_temp,"%s",strtok(NULL,","));  //longitude
strcpy(gps.longitude, token_temp);
//sprintf(token_temp,"%s",strtok(NULL,","));   //hemisphere
}
else
{
//strcpy(gps.status,"no_data");
strcpy(gps.latitude,"0");
strcpy(gps.longitude,"0");
}
}
gps.l_long = (long)(1000*atof(gps.longitude));
gps.l_lat = (long)(1000*atof(gps.latitude));
#ifdef debug_message
printf("GPS: %ld %ld\n", gps.l_lat, gps.l_long);
#endif
buff_gps[0] = gps.l_lat>>24;
buff_gps[1] = gps.l_lat>>16;
buff_gps[2] = gps.l_lat>>8;
buff_gps[3] = gps.l_lat;
buff_gps[4] = gps.l_long>>24;
buff_gps[5] = gps.l_long>>16;
buff_gps[6] = gps.l_long>>8;
buff_gps[7] = gps.l_long;
sendto(socket_gps, buff_gps, BUFF_GPS_LEN, 0, (struct sockaddr *) &myaddr_gps, sizeof(myaddr_gps));
usleep(GPS_SLEEP);
}
//restore the old port settings
tcsetattr(fd,TCSANOW,&oldtio);
}
*/

//low level functions
//=====================================================
int set_motor_low_res(int hUSB,char speed1,char speed2)
{
    unsigned int n;
    uchar sbuf[20];
    int ress=0;
    uchar tt=0;
    sbuf[0] = 255;
    sbuf[1] = 0x07;
    int tmp1 = 8*(speed1&0x3F);
    int tmp2 = 8*(speed2&0x3F);
    if (speed2&0x80) tt=tt+32;
    if (speed2&0x40) tt=tt+16;
    sbuf[2] = (uchar)tmp1;
    sbuf[3] = (uchar)(tmp1 >> 8);
    sbuf[4] = (uchar)tmp2;
    sbuf[5] = (uchar)(tmp2 >> 8);
    sbuf[6] = (uchar)((speed1&0x80) + (speed1&0x40) + tt);
    short mycrcsend = crc16(sbuf+1,6);
    sbuf[7] = (uchar)mycrcsend;
    sbuf[8] = (uchar)(mycrcsend >> 8);
    ress = writers232(hUSB, sbuf, 9,&n);
    return ress;
}

//===============================================================
int set_motor(int hUSB,short speed1,short speed2,uchar SpeedFlag)
{
    unsigned int n;
    uchar sbuf[20];
    sbuf[0] = 255;
    sbuf[1] = 0x07;
    sbuf[2] = (uchar)speed1;
    sbuf[3] = (uchar)(speed1 >> 8);
    sbuf[4] = (uchar)speed2;
    sbuf[5] = (uchar)(speed2 >> 8);
    sbuf[6] = SpeedFlag;
    short mycrcsend = crc16(sbuf+1,6);
    sbuf[7] = (uchar)mycrcsend;
    sbuf[8] = (uchar)(mycrcsend >> 8);
    return writers232(hUSB, sbuf, 9,&n);
}

//===================================================================================================
int set_motor_pid(int hUSB, uchar speed1, uchar speed2, uchar pp, uchar ii, uchar dd, short maxspeed)
{
    unsigned int n;
    uchar sbuf[20];
    sbuf[0] = 255;
    sbuf[1] = 0x09;
    sbuf[2] = speed1;
    sbuf[3] = speed2;
    sbuf[4] = pp;
    sbuf[5] = ii;
    sbuf[6] = dd;
    sbuf[7] = (uchar)maxspeed;
    sbuf[8] = (uchar)(maxspeed >> 8);
    short mycrcsend = crc16(sbuf+1,8);
    sbuf[9] = (uchar)mycrcsend;
    sbuf[10] = (uchar)(mycrcsend >> 8);
    return writers232(hUSB, sbuf, 11, &n);
}

//======================
int stop_motor(int hUSB)
{
    set_motor_low_res(hUSB,0x00,0x00);
    return 1;
}

//==========================
int get_motor_buff(int hUSB)
{
    int i, ress=0, r=0;
    unsigned int n;
    uchar sbuf[BUFF_OUT_LEN];

    do { // Sync
        r = readrs232(hUSB,sbuf, 1, &n);
    } while (sbuf[0] != 255 && r);
    if (r) r = readrs232(hUSB,sbuf, 21, &n);

    short mycrcrcv = (short)((sbuf[20] << 8) + sbuf[19]);
    short mycrcsend = crc16(sbuf,19);

    if (mycrcrcv!=mycrcsend)
    {
        do { // Sync
            r = readrs232(hUSB,sbuf, 1, &n);
        } while (sbuf[0] != 255 && r);
    }
    else
    {
        for(i=0;i<BUFF_OUT_LEN;i++)
            buff_out[i]=sbuf[i];
    }
    return ress;
}

//=============================================
int openrs232(const char *device, int baudrate)
{
    struct termios  options;
    int bitrate;
    //if (hUSB)
    //return 0;
    bitrate = (baudrate == 9600) ? B9600 : (baudrate == 19200) ? B19200 : 0;
    if (!bitrate)
    {
        sprintf(file_line,"ERROR : Serial %d baudrate unsupported!\n", baudrate);
        file_write(file_line);
        return 0;
    }
    hUSB = open(device, O_RDWR | O_NOCTTY);
    if (hUSB < 0)
    {
        hUSB = 0;
        sprintf(file_line,"ERROR : Serial\n");
        file_write(file_line);
        return 0;
    }
    tcgetattr(hUSB, &options);
    options.c_cflag = bitrate | CS8 | CREAD | CLOCAL;
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    options.c_cc[VMIN]  = 1;
    options.c_cc[VTIME] = 0;
    tcsetattr(hUSB, TCSANOW, &options);
    return hUSB;
}

//==========================================================================================================
int readrs232(int hUSB, uchar *buffer, unsigned int nNumberOfBytesToRead, unsigned int *lpNumberOfBytesRead)
{
    int kk=0;
    if (!hUSB)
        return -1;
    *lpNumberOfBytesRead  = 0;
    for (kk = 0; kk < nNumberOfBytesToRead; kk++)
        *lpNumberOfBytesRead += read(hUSB, buffer + kk, 1);
    //  *lpNumberOfBytesRead = ::read(m_handle, buffer, nNumberOfBytesToRead);
    return (nNumberOfBytesToRead >= 0);
}

//==============
int closers232()
{
    if (!hUSB)
        return 0;
    close(hUSB);
    hUSB = 0;
    return 1;
}

//===============================================================================================================
int writers232(int hUSB, uchar *buffer, unsigned int nNumberOfBytesToWrite, unsigned int *lpNumberOfBytesWritten)
{
    if (!hUSB)
        return 0;
    *lpNumberOfBytesWritten = 0;
    int kk=0;
    for (kk = 0; kk < nNumberOfBytesToWrite; kk++)
        *lpNumberOfBytesWritten += write(hUSB, buffer + kk, 1);
    //*lpNumberOfBytesWritten = ::write(m_handle, buffer, nNumberOfBytesToWrite);
    return (*lpNumberOfBytesWritten >= 0);
}

//================================================
short crc16(uchar *adresse_tab , uchar taille_max)
{
    unsigned int Crc = 0xFFFF;
    unsigned int Polynome = 0xA001;
    unsigned int CptOctet = 0;
    unsigned int CptBit = 0;
    unsigned int Parity= 0;
    Crc = 0xFFFF;
    Polynome = 0xA001; // Polynôme = 2^15 + 2^13 + 2^0 = 0xA001.
    for ( CptOctet= 0 ; CptOctet < taille_max ; CptOctet++)
    {
        Crc ^= *( adresse_tab + CptOctet); //Ou exculsif entre octet message et CRC
        for ( CptBit = 0; CptBit <= 7 ; CptBit++) /* Mise a 0 du compteur nombre de bits */
        {
            Parity= Crc;
            Crc >>= 1; // Décalage a droite du crc
            if (Parity%2 == 1) Crc ^= Polynome; // Test si nombre impair -> Apres decalage à droite il y aura une retenue
        } // "ou exclusif" entre le CRC et le polynome generateur.
    }
    return(Crc);
}

//==================================
void file_write(char *line_to_write)
{
    gettimeofday(&tv, &tz);
    fprintf(file_out,"%ld.%06ld %s",tv.tv_sec,tv.tv_usec,line_to_write);
}

//==================================
char checksum(uchar *buff, char len)
{
    uchar check_sum=0;
    int i;
    for(i=0;i<len;i++) check_sum+=buff[i];
    if(check_sum==0)
        return 1;
    else
        return 0;
}

//======================================
char add_checksum(uchar *buff, char len)
{
    uchar check_sum=0;
    int i;
    for(i=0;i<(len-1);i++) check_sum+=buff[i];
    check_sum=-check_sum;
    return check_sum;
}
