# coding: utf-8

import serial

class Wifibot():


    def __init__(self, port="/dev/ttyS0", baudrate=19200):
        self.port = port
        self.baudrate = baudrate
        self.ser = serial.Serial(port, baudrate)

    def speed():
        Exception("Get what's the speed of the robot")

    def read(raw=False):
        if raw:
            Exception("read information from binary input directly")
        else:
            Exception("read information in an interpreted way")


    def write(raw=False):
        if raw:
            Exception("Write data in a binary form")
        else:
            Exception("Write human readable data")

    def move(self, angle=0, speed=0):
        Exception("Fait bouger le robot")


    def rotate(self, angle):
        self.move(angle=angle, speed=0)

def crc16(data, taille_max):
    crc = 0xffff
    polynome = 0xa001
    parity = 0
    for cpt in range(taille_max):
        crc ^= data[cpt]
        for bit in range(7):
            parity = crc
            crc >>= 1
            if parity % 2 == 0:
                crc ^= polynome
    return crc


wifibot = Wifibot()
