// MOVE: Small program for Wifibot movement control
// September 2011
// Milan Erdelj (milan.erdelj@inria.fr)

#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <time.h>
#include <sys/time.h>
#include <malloc.h>
#include <arpa/inet.h>

typedef unsigned char uchar;

//#define debug_printf

#define THREAD_COUNT 2
#define DISPLAY_COUNTER 10

#define PORT_COMMAND 15000
#define PORT_STATUS 15050
//#define PORT_GPS 15055
#define HOST_ADDR "127.0.0.1"

#define MAIN_SLEEP 255000
#define STATUS_SLEEP 5000
//#define GPS_SLEEP 1000000
#define MOTOR_SLEEP 50000

#define STATUS_LEN 21
#define COMMAND_LEN 5
//#define GPS_LEN 8

#define TICKS_PER_CM 56.31
#define TICKS_PER_CIRCLE 6871.5
#define WHEEL_BASE 35

#define INERTION 10
#define TARGET_PROXIMITY 10
#define SENSOR_TRESHOLD 120
#define ANGLE_TRESHOLD 10
#define ANGLE_OVERFLOW 25
#define ANGLE_DIFF 10
#define ANGLE_CORRECTION 1.5

#define PID_P 95
#define PID_I 0
#define PID_D 45
#define MAX_SPEED 140
#define ROTATION_SPEED 75
#define ACCELERATION 10
#define FLAG_FORWARD 0xF0   // 11110000
#define FLAG_LEFT 0xB0      // 10110000
#define FLAG_RIGHT 0xE0     // 11100000
#define FLAG_BACKWARD 0xA0  // 10100000

typedef struct
{
    long odom;
    int speed;
    uchar voltage;
    uchar IR1, IR2;
} SensorData;

typedef struct
{
    SensorData left, right;
    uchar current;
    uchar version;
    float pos_x, pos_y, angle;
    float dist_from_start;
    long latitude, longitude;
} RobotData;

RobotData me;

float target_angle=0, target_distance=0;
long last_lsamp, last_rsamp;
int moving_allowed=1;

pthread_t threads[THREAD_COUNT];
void * th_robot_status(void *num);
//void * th_gps(void *num);

char *host_addr="127.0.0.1";
uchar buff_command[COMMAND_LEN];
struct sockaddr_in myaddr_command, client_command;
int socket_command;
int wait_for_status=0;
int input_speed=MAX_SPEED;

struct timeval tv;
struct timezone tz;
struct tm *tm;

int process_args(int arg_count, char *arguments[]);
void rotate();
void go_forward();
void update_status_data(uchar *buffer);
void set_motors_pid(uchar p, uchar i, uchar d);
void set_motors_speed_dir(uchar lspeed, uchar rspeed, uchar direction);
void stop_motors();
float to_rad(float);
float to_deg(float);
uchar checksum(uchar *buff, uchar len);
uchar add_checksum(uchar *buff, uchar len);
inline float distance_between(float x1, float y1, float x2, float y2);

//==============================
//==============================
int main(int argc, char *argv[])
{
    int ret;
    //printf("[wifibot - go]\n");
    /*
       if(argc==4)
       {
       strcpy(host_addr,argv[1]);
       target_distance=100*atof(argv[2]); //convert to cm
       target_angle=atof(argv[3]);
       }
       else
       {
       printf("syntax: go [address] [distance] [angle]\n");
       exit(1);
       }
       */
    if(process_args(argc, argv)<0)
    {
        printf("Syntax: move -h <address> -d <distance> -a <angle> -s <speed>\n");
        exit(1);
    }
    if((socket_command=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP))<0)
    {
        fprintf(stderr,"Command socket error\n");
        exit(1);
    }
    memset((char *) &myaddr_command, 0, sizeof(myaddr_command));
    myaddr_command.sin_family=AF_INET;
    myaddr_command.sin_port=htons(PORT_COMMAND);
    myaddr_command.sin_addr.s_addr=inet_addr(host_addr); //inet_addr(HOST_ADDR);
    stop_motors();
    if((ret = pthread_create(&threads[0], NULL, th_robot_status, (void *) 0)) != 0)
    {
        fprintf(stderr,"Status thread (%s)\n", strerror(ret));
        exit (1);
    }
    /*
       if((ret = pthread_create(&threads[1], NULL, th_gps, (void *) 0)) != 0)
       {
       printf("ERRR gps thread (%s)\n", strerror(ret));
       exit (1);
       }
       */
    while(!wait_for_status)
    {
        usleep(50000);
    }
    // do the thing and exit
    //printf("DIST_START: %.2f\n",target_distance);
    me.dist_from_start=0;
    rotate();
    go_forward();
    printf("[wifibot] %.2f\n",me.dist_from_start/100);
    return 0;
}

//======================================
int process_args(int argc, char *argv[])
{
    char *param_list = "h:d:a:s:?"; // server_address, distance, angle, speed
    int option;
    //char *host_addr = "127.0.0.1";
    char *recv_distance = "0";
    char *recv_angle = "0";
    char *recv_speed = "200";
    //char *port = "PORT_COMMAND";
    //struct hostent *hostent;
    //struct servent *servent;
    if(argc==1)
    {
        return -1;
    }
    else
    {
        while((option = getopt(argc, argv, param_list)) != -1)
        {
            switch(option)
            {
                case 'h':
                    host_addr = optarg;
                    break;
                case 'd' :
                    recv_distance = optarg;
                    break;
                case 'a' :
                    recv_angle = optarg;
                    break;
                case 's' :
                    recv_speed = optarg;
                    break;
                case '?' :
                    printf(" Syntax: move -h <host_adress> -d <distance> -a <angle> -s <speed>\n");
                    printf("Example: move -h 192.168.1.55 -d 5 -a 45 -s 120\n");
                    printf("  Infos: milan.erdelj@gmail.com\n");
                    break;
            }
        }
#ifdef debug_printf
        printf("ARGS: host %s  dist %s  angle %s\n", host_addr, recv_distance, recv_angle);
#endif
        // check the angle TODO
        target_angle=atof(recv_angle);
        // check the distance TODO
        target_distance=100*atof(recv_distance); //convert to cm
        // check the address TODO
        input_speed = atof(recv_speed);
        return 0;
    }
}

//===========
void rotate()
{
    uchar speed_flag;
    target_angle = ((long)target_angle) % 360;
#ifdef debug_printf
    printf("ROT: deg %.2f", target_angle);
#endif
    target_angle = to_rad(target_angle);
    //target_angle = target_angle%(2*M_PI);
    if(target_angle>=0)
    {
        if(target_angle<=M_PI)
        {
            speed_flag=FLAG_RIGHT;
        }
        else
        {
            speed_flag=FLAG_LEFT;
            target_angle=2*M_PI-target_angle;
        }
    }
    else
    {
        target_angle=-target_angle;
        if(target_angle<=M_PI)
        {
            speed_flag=FLAG_LEFT;
        }
        else
        {
            speed_flag=FLAG_RIGHT;
            target_angle=2*M_PI-target_angle;
        }
    }
#ifdef debug_printf
    printf(" rad %.2f flag %d\n", target_angle, speed_flag);
#endif
    if(target_angle>(to_rad(ANGLE_TRESHOLD)))
    {
        //set_motors_speed_dir(ROTATION_SPEED,ROTATION_SPEED, speed_flag);
        while((target_angle-abs(me.angle))>to_rad(ANGLE_OVERFLOW))	//including inertion overturn ~23.3 degrees (speed~70)
        {
            set_motors_speed_dir(ROTATION_SPEED, ROTATION_SPEED, speed_flag);
            usleep(MOTOR_SLEEP);
        }
        stop_motors();
    }
}

//===============
void go_forward()
{
    float angle_diff;
    uchar lspeed, rspeed, speed_flag, current_speed=0;
#ifdef debug_printf
    printf("MOV: %f\n",target_distance);
#endif
    if(target_distance>=0)
    {
        speed_flag=FLAG_FORWARD;
    }
    else
    {
        target_distance=-target_distance;
        speed_flag=FLAG_BACKWARD;
    }
    if(target_distance>INERTION)
    {
        while(moving_allowed&&(me.dist_from_start<(target_distance-INERTION)))
        {
            //acceleration
            if(current_speed<input_speed)
            {
                current_speed += ACCELERATION;
                if(current_speed>input_speed)
                    current_speed = input_speed;
                lspeed=rspeed=current_speed;
            }
            else
            {
                lspeed=rspeed=input_speed;
                // speed-trajectory correction
                angle_diff = target_angle-me.angle;
                if(angle_diff>to_rad(ANGLE_DIFF))
                {
                    if(speed_flag==FLAG_FORWARD)
                    {
                        lspeed += 5;  //lspeed += (2*ANGLE_CORRECTION*angle_diff)/M_PI;
                        rspeed -= 15; //rspeed -= (2*ANGLE_CORRECTION*angle_diff)/M_PI;
                    }
                    if(speed_flag==FLAG_BACKWARD)
                    {
                        rspeed += 5;
                        lspeed -= 15;
                    }
                }
                if(angle_diff<-to_rad(ANGLE_DIFF))
                {
                    if(speed_flag==FLAG_FORWARD)
                    {
                        rspeed += 5;  //rspeed += (2*ANGLE_CORRECTION*angle_diff)/M_PI;
                        lspeed -= 15; //lspeed -= (2*ANGLE_CORRECTION*angle_diff)/M_PI;
                    }
                    if(speed_flag==FLAG_BACKWARD)
                    {
                        lspeed += 5;
                        rspeed -= 15;
                    }

                }
            }
            set_motors_speed_dir(lspeed, rspeed, speed_flag);
            usleep(MOTOR_SLEEP);
        }
        // decceleration
        while(moving_allowed&&(current_speed>0))
        {
            current_speed -= ACCELERATION<<1;
            if(current_speed<0)
                current_speed = 0;
            lspeed = rspeed = current_speed;
        }
        stop_motors();
    }
}

//===============================
void * th_robot_status(void *num)
{
    //#ifdef debug_printf
    int display_counter=DISPLAY_COUNTER;
    //#endif
    int socket_status;
    float l_cm, r_cm, sum_cm;
    long lsamp, rsamp, lticks, rticks;
    uchar buff_status[STATUS_LEN];
    struct sockaddr_in myaddr_status, client_status;
    socklen_t clilen_status=sizeof(client_status);
    if((socket_status=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP))<0)
    {
        printf("status socket error\n");
        exit(1);
    }
    memset((char *)&myaddr_status, 0, sizeof(myaddr_status));
    myaddr_status.sin_family=AF_INET;
    myaddr_status.sin_addr.s_addr=inet_addr(host_addr);
    myaddr_status.sin_port=htons(PORT_STATUS);
    if(bind(socket_status,(struct sockaddr *)&myaddr_status,sizeof(myaddr_status))<0)
    {
        printf("status socket bind error\n");
        exit(1);
    }
#ifdef debug_printf
    printf("status thread OK\n");
#endif
    recvfrom(socket_status,buff_status,STATUS_LEN,0,(struct sockaddr *)&client_status,&clilen_status);
#ifdef debug_printf
    printf("after recfrom\n");
#endif
    update_status_data(buff_status);
    last_lsamp = me.left.odom;
    last_rsamp = me.right.odom;
    me.pos_x=me.pos_y=0;
    me.angle=0;
    wait_for_status=1;
    while(1)
    {
        recvfrom(socket_status,buff_status,STATUS_LEN,0,(struct sockaddr *)&client_status,&clilen_status);
        update_status_data(buff_status);
        //#ifdef debug_printf
        if(!(--display_counter))
        {
            /*
               printf("STAT:\n");
               printf("L - speed %d  vol %d  IR1 %d  IR2 %d odom %ld\n", me.left.speed, me.left.voltage, me.left.IR1, me.left.IR2, me.left.odom);
               printf("R - speed %d  vol %d  IR1 %d  IR2 %d odom %ld\n", me.right.speed, me.right.voltage, me.right.IR1, me.right.IR2, me.right.odom);
               printf("    current %d  version %d\n", me.current, me.version);
               printf("DIST: %.2f\n",me.dist_from_start);
               printf(" ANG: %.2f\n",me.angle);
               */
            gettimeofday(&tv, &tz);
            printf("%ld.%06ld [GO] %d %d %d %d %d %d %d %d %d %ld %ld %d\n", tv.tv_sec, tv.tv_usec, me.left.speed, me.right.speed, me.right.voltage, me.left.voltage, me.current, me.left.IR1, me.left.IR2, me.right.IR1, me.right.IR2, me.left.odom, me.right.odom, me.version);
            display_counter=DISPLAY_COUNTER;
        }
        //#endif
        if((me.left.IR1>SENSOR_TRESHOLD)||(me.right.IR1>SENSOR_TRESHOLD))
            moving_allowed=0;
        lsamp = me.left.odom;
        rsamp = me.right.odom;
        lticks = lsamp - last_lsamp;
        rticks = rsamp - last_rsamp;
        last_lsamp = lsamp;
        last_rsamp = rsamp;
        l_cm = (float)lticks/TICKS_PER_CM;
        r_cm = (float)rticks/TICKS_PER_CM;
        sum_cm = (l_cm + r_cm)/2;
        me.angle += ((lticks - rticks)/TICKS_PER_CIRCLE)*M_PI;
        if(me.angle>M_PI) me.angle-=2*M_PI;
        if(me.angle<-M_PI) me.angle+=2*M_PI;
        me.pos_y += sum_cm * cos(me.angle);
        me.pos_x += sum_cm * sin(me.angle);
        me.dist_from_start = distance_between(me.pos_x,me.pos_y,0,0);
        //target_angle = atan2(((float)target_x-me.pos_x),((float)target_y-me.pos_y));
        //target_distance = (long)distance_between(target_x,target_y,me.pos_x,me.pos_y);
        usleep(STATUS_SLEEP);
    }
}

/*
//======================
void * th_gps(void *num)
{
uchar buff_gps[GPS_LEN];
int socket_gps;
struct sockaddr_in myaddr_gps, client_gps;
socklen_t clilen_gps=sizeof(client_gps);
if((socket_gps=socket(AF_INET,SOCK_DGRAM,0))<0)
{
fprintf(stderr,"Gps socket error\n");
exit(1);
}
memset((char *)&myaddr_gps, 0, sizeof(myaddr_gps));
myaddr_gps.sin_family=AF_INET;
myaddr_gps.sin_addr.s_addr=inet_addr(HOST_ADDR);
myaddr_gps.sin_port=htons(PORT_GPS);
if(bind(socket_gps,(struct sockaddr *)&myaddr_gps,sizeof(myaddr_gps))<0)
{
fprintf(stderr,"Gps socket bind error\n");
exit(1);
}
#ifdef debug_printf
printf("gps thread OK\n");
#endif
while(1)
{
recvfrom(socket_gps,buff_gps,GPS_LEN,0,(struct sockaddr *)&client_gps,&clilen_gps);
me.latitude = ((long)buff_gps[0]<<24) + ((long)buff_gps[1]<<16) + ((long)buff_gps[2]<<8) + (long)buff_gps[3];
me.longitude = ((long)buff_gps[4]<<24) + ((long)buff_gps[5]<<16) + ((long)buff_gps[6]<<8) + (long)buff_gps[7];
#ifdef debug_printf
printf("GPS  lat: %ld (%d %d %d %d)\n", me.latitude, buff_gps[0], buff_gps[1], buff_gps[2], buff_gps[3]);
printf("GPS long: %ld (%d %d %d %d)\n", me.longitude, buff_gps[4], buff_gps[5], buff_gps[6], buff_gps[7]);
#endif
usleep(GPS_SLEEP);
}
}
*/

//====================================
void update_status_data(uchar *buffer)
{
    me.left.speed=(int)(((int)buffer[1]<<8) + buffer[0]);
    if(me.left.speed>32767)
        me.left.speed-=65536;
    me.left.speed=me.left.speed;
    me.right.voltage=buffer[1];
    me.left.voltage=buffer[2];
    me.left.IR1=buffer[3];
    me.left.IR2=buffer[4];
    me.left.odom=((((long)buffer[8]<<24))+(((long)buffer[7]<<16))+(((long)buffer[6]<<8))+((long)buffer[5]));
    me.right.speed=(int)((buffer[10] << 8) + buffer[9]);
    if (me.right.speed > 32767)
        me.right.speed-=65536;
    me.right.speed=me.right.speed;
    me.right.IR1=buffer[11];
    me.right.IR2=buffer[12];
    me.right.odom=((((long)buffer[16]<<24))+(((long)buffer[15]<<16))+(((long)buffer[14]<<8))+((long)buffer[13]));
    me.current=buffer[17];
    me.version=buffer[18];
}

//===================================================================
void set_motors_speed_dir(uchar lspeed, uchar rspeed, uchar direction)
{
#ifdef debug_printf
    printf("SET: speed %d %d  dir %d\n",lspeed, rspeed, direction);
#endif
    buff_command[0]=2;
    buff_command[1]=lspeed;
    buff_command[2]=rspeed;
    buff_command[3]=direction;
    buff_command[4]=add_checksum(buff_command, COMMAND_LEN);
    sendto(socket_command, buff_command, COMMAND_LEN, 0, (struct sockaddr *) &myaddr_command, sizeof(myaddr_command));
}

//================
void stop_motors()
{
    set_motors_speed_dir(0, 0, FLAG_FORWARD);
    usleep(15000);
}

//===========================================
void set_motors_pid(uchar p, uchar i, uchar d)
{
    buff_command[0]=3;
    buff_command[1]=p;
    buff_command[2]=i;
    buff_command[3]=d;
    buff_command[4]=add_checksum(buff_command, COMMAND_LEN);
    sendto(socket_command, buff_command, COMMAND_LEN, 0, (struct sockaddr *) &myaddr_command, sizeof(myaddr_command));
}

/*
//===============
void send_hello()
{
buff_command[0]=buff_command[1]=buff_command[2]=buff_command[3]=1;
buff_command[4]=add_checksum(buff_command, COMMAND_LEN);
sendto(socket_command, buff_command, COMMAND_LEN, 0, (struct sockaddr *) &myaddr_command, sizeof(myaddr_command));
}
*/

//===================================================================
inline float distance_between(float x1, float y1, float x2, float y2)
{
    return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
}

//===========================
float to_rad(float value_deg)
{
    return (value_deg*M_PI)/180;
}

//===========================
float to_deg(float value_rad)
{
    return (value_rad*180)/M_PI;
}

//====================================
uchar checksum(uchar *buff, uchar len)
{
    uchar check_sum=0;
    int i;
    for(i=0;i<len;i++) check_sum+=buff[i];
    if(check_sum==0)
        return 1;
    else
        return 0;
}

//========================================
uchar add_checksum(uchar *buff, uchar len)
{
    uchar check_sum=0;
    int i;
    for(i=0;i<(len-1);i++) check_sum+=buff[i];
    check_sum=-check_sum;
    return check_sum;
}
